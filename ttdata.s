; TIATracker music player
; Copyright 2016 Andre "Kylearan" Wichmann
; Website: https://bitbucket.org/kylearan/tiatracker
; Email: andre.wichmann@gmx.de
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;   http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

.include "globals.inc"

.segment "RODATA"

tt_InsCtrlTable:
        .byte $07, $04, $0c, $06

tt_InsADIndexes:
        .byte $00, $0a, $0a, $0f

tt_InsSustainIndexes:
        .byte $00, $0a, $0a, $0f

tt_InsReleaseIndexes:
        .byte $01, $0b, $0b, $10

tt_InsFreqVolTable:
; 0: Melo03
        .byte $86, $00, $86, $8a, $86, $86, $86, $86
        .byte $86, $00
; 1+2: Melo01
        .byte $87, $00, $06, $06, $00
; 3: Bass01
        .byte $87, $00, $87, $00

tt_PercIndexes:
        .byte $01, $04, $0f

tt_PercFreqTable:
; 0: Hat
        .byte $02, $83, $00
; 1: Kick
        .byte $03, $01, $02, $03, $04, $05, $06, $07
        .byte $08, $09, $00
; 2: Snare
        .byte $05, $1d, $08, $05, $05, $05, $00

tt_PercCtrlVolTable:
; 0: Hat
        .byte $84, $82, $00
; 1: Kick
        .byte $6e, $ee, $ee, $ee, $e9, $e8, $e8, $e6
        .byte $e6, $e6, $00
; 2: Snare
        .byte $8f, $cf, $6f, $89, $86, $83, $00

; intro1x
tt_pattern0:
        .byte $11, $08, $08, $08, $11, $08, $08, $08
        .byte $11, $08, $08, $08, $11, $08, $08, $08
        .byte $00

; intro2
tt_pattern1:
        .byte $11, $08, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $08
        .byte $00

; left01X
tt_pattern2:
        .byte $11, $08, $11, $08, $11, $08, $11, $11
        .byte $11, $11, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $11
        .byte $11, $11, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $11
        .byte $11, $11, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $11
        .byte $11, $11, $11, $08, $11, $08, $11, $08
        .byte $00

; left01X
tt_pattern3:
        .byte $11, $08, $11, $08, $11, $08, $11, $11
        .byte $11, $11, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $11
        .byte $11, $11, $11, $08, $11, $08, $12, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $11
        .byte $11, $11, $11, $08, $11, $08, $11, $08
        .byte $11, $08, $11, $08, $11, $08, $11, $11
        .byte $11, $11, $11, $08, $11, $08, $11, $08
        .byte $00

; left09
tt_pattern4:
        .byte $32, $08, $11, $08, $32, $08, $11, $08
        .byte $32, $10, $34, $10, $32, $10, $2f, $08
        .byte $32, $08, $11, $08, $32, $08, $11, $08
        .byte $32, $10, $34, $10, $32, $10, $2f, $10
        .byte $32, $08, $11, $08, $32, $08, $11, $08
        .byte $32, $10, $34, $10, $32, $10, $2f, $10
        .byte $3b, $08, $11, $08, $3b, $08, $10, $08
        .byte $38, $08, $11, $08, $38, $08, $11, $08
        .byte $00

; left09
tt_pattern5:
        .byte $32, $08, $11, $08, $32, $08, $11, $08
        .byte $32, $10, $34, $10, $32, $10, $2f, $08
        .byte $32, $08, $11, $08, $32, $08, $11, $08
        .byte $32, $10, $34, $10, $32, $10, $2f, $10
        .byte $32, $08, $11, $08, $32, $08, $11, $08
        .byte $32, $10, $34, $10, $32, $10, $2f, $10
        .byte $3b, $08, $11, $08, $3b, $08, $10, $08
        .byte $38, $08, $11, $08, $38, $08, $11, $08
        .byte $00

; left05
tt_pattern6:
        .byte $77, $08, $10, $08, $77, $08, $10, $08
        .byte $08, $08, $73, $08, $10, $08, $73, $08
        .byte $77, $08, $10, $08, $77, $08, $10, $08
        .byte $08, $08, $73, $08, $10, $08, $73, $08
        .byte $77, $08, $10, $08, $77, $08, $10, $08
        .byte $08, $08, $73, $08, $10, $08, $77, $08
        .byte $71, $08, $10, $08, $71, $08, $10, $08
        .byte $6f, $08, $10, $08, $6f, $08, $10, $08
        .byte $00

; left06
tt_pattern7:
        .byte $77, $08, $10, $08, $77, $08, $10, $08
        .byte $08, $08, $73, $08, $10, $08, $73, $08
        .byte $77, $08, $10, $08, $77, $08, $10, $08
        .byte $08, $08, $73, $08, $10, $08, $73, $08
        .byte $77, $08, $10, $08, $77, $08, $10, $08
        .byte $08, $08, $73, $08, $10, $08, $77, $08
        .byte $71, $08, $10, $08, $71, $08, $10, $08
        .byte $6f, $08, $10, $08, $6f, $08, $10, $08
        .byte $00

; silence
tt_pattern8:
        .byte $08, $08, $08, $08, $08, $08, $08, $08
        .byte $08, $08, $08, $08, $08, $08, $08, $08
        .byte $00

; right02
tt_pattern9:
        .byte $12, $08, $08, $08, $12, $08, $08, $08
        .byte $12, $08, $08, $08, $12, $08, $08, $08
        .byte $12, $08, $08, $08, $12, $08, $08, $08
        .byte $12, $08, $08, $08, $12, $08, $12, $08
        .byte $12, $08, $08, $08, $12, $08, $08, $08
        .byte $12, $08, $08, $08, $12, $08, $08, $08
        .byte $12, $08, $08, $08, $12, $08, $08, $08
        .byte $12, $08, $08, $08, $12, $08, $12, $08
        .byte $00

; right03
tt_pattern10:
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $12, $08
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $8d, $10, $12, $08, $8d, $10
        .byte $12, $08, $98, $10, $13, $08, $12, $08
        .byte $00

; right05
tt_pattern11:
        .byte $12, $08, $92, $10, $13, $08, $92, $10
        .byte $12, $08, $92, $10, $13, $08, $92, $10
        .byte $12, $08, $92, $10, $13, $08, $92, $10
        .byte $12, $08, $92, $10, $13, $08, $13, $08
        .byte $12, $08, $08, $08, $13, $08, $92, $10
        .byte $12, $08, $92, $10, $13, $08, $92, $10
        .byte $12, $08, $8d, $10, $13, $08, $8d, $10
        .byte $12, $08, $98, $10, $13, $08, $13, $08
        .byte $00

; right04
tt_pattern12:
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $12, $08
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $8d, $10, $12, $08, $8d, $10
        .byte $12, $08, $98, $10, $13, $08, $13, $08
        .byte $00

; right04
tt_pattern13:
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $12, $08
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $92, $10, $12, $08, $92, $10
        .byte $12, $08, $8d, $10, $12, $08, $8d, $10
        .byte $12, $08, $98, $10, $13, $08, $13, $08
        .byte $00

tt_PatternPtrLo:
        .byte <tt_pattern0, <tt_pattern1, <tt_pattern2, <tt_pattern3
        .byte <tt_pattern4, <tt_pattern5, <tt_pattern6, <tt_pattern7
        .byte <tt_pattern8, <tt_pattern9, <tt_pattern10, <tt_pattern11
        .byte <tt_pattern12, <tt_pattern13
tt_PatternPtrHi:
        .byte >tt_pattern0, >tt_pattern1, >tt_pattern2, >tt_pattern3
        .byte >tt_pattern4, >tt_pattern5, >tt_pattern6, >tt_pattern7
        .byte >tt_pattern8, >tt_pattern9, >tt_pattern10, >tt_pattern11
        .byte >tt_pattern12, >tt_pattern13        

tt_SequenceTable:
        ; ---------- Channel 0 ----------
        .byte $00, $00, $00, $00, $01, $02, $03, $04
        .byte $05, $06, $07, $87

        
        ; ---------- Channel 1 ----------
        .byte $08, $08, $08, $08, $08, $08, $08, $08
        .byte $08, $08, $08, $08, $09, $0a, $0b, $0c
        .byte $0d, $99
