
.include "vcs.inc"
.include "globals.inc"

.segment "RODATA"

message0:
   .byte $00,$67,$54,$56,$54,$67,$00,$2a,$2a,$13,$2a,$2b,$00,$1d,$11,$11
   .byte $11,$11
message1:
   .byte $00,$56,$55,$75,$55,$76,$00,$92,$92,$93,$aa,$ab,$00,$dd,$44,$5d
   .byte $51,$dd
message2:
   .byte $00,$75,$45,$45,$45,$45,$00,$a8,$a8,$90,$a9,$a8,$00,$dc,$54,$d4
   .byte $14,$dc
message3:
   .byte $00,$2e,$28,$6c,$a8,$2e,$00,$20,$70,$f8,$fc,$d8,$00,$68,$90,$48
   .byte $a0,$40

.segment "CODE"

showmessage:
   sta   COLUP0
   sta   COLUP1
   lda   #$44
   jsr   spritepos
   ldx   #$01
   stx   NUSIZ0
   stx   NUSIZ1
   ldy   #<(message1-message0-1)
   
@loop:
   sta   WSYNC
   ldx   message3,y
   lda   message0,y
   sta   GRP0
   lda   message1,y
   sta   GRP1
   lda   message2,y
   nop
   nop
   nop
   nop
   nop
   jsr   @sleep12
   sta   GRP0
   stx   GRP1
   dey
   bpl   @loop

@sleep12:
   rts
