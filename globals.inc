
; configuration parameters

.ifndef NTSC
.define NTSC 0
.endif

.if NTSC
; NTSC values
.define splashtype 1
.define TIMER_VBLANK   $20
.define TIMER_SCREEN   $10
.define TIMER_OVERSCAN $12

.define PCOL1     $d2
.define PCOL2     $d0
.define PCOL3     $80
.define BGCOL1    $dc
.define BGCOL2    $da
.define BGCOL3    $a4
.define BGCOL4    $a2

.define HEIGHT1   $16
.define HEIGHT2   $1f
.define HEIGHT3   $05
.define HEIGHT4   $10

TT_SPEED                = 5
TT_ODD_SPEED            = 5
.else
; PAL values

; 0=off,palette correction:1=none,2=svolli,3=omegamatrix eor,4=omegamatrix ora
.define splashtype 2
.define TIMER_VBLANK   $2a
.define TIMER_SCREEN   $13
.define TIMER_OVERSCAN $14

.define PCOL1     $32
.define PCOL2     $30
.define PCOL3     $d0
.define BGCOL1    $3c
.define BGCOL2    $3a
.define BGCOL3    $94
.define BGCOL4    $92

.define HEIGHT1   $26
.define HEIGHT2   $27
.define HEIGHT3   $0d
.define HEIGHT4   $18

; timer values for vblank and overscan are for TIM64TI, screen is for TIM1KTI
; PAL values, determined by trial an error

; tia tracker config
; duration (number of TV frames) of a note
TT_SPEED                = 4
; duration of odd frames (needs TT_USE_FUNKTEMPO)
TT_ODD_SPEED            = 4
.endif
; 1: Global song speed, 0: Each pattern has individual speed
TT_GLOBAL_SPEED         = 1

; 1: Overlay percussion, +40 bytes
TT_USE_OVERLAY          = 1
; 1: Melodic instrument slide, +9 bytes
TT_USE_SLIDE            = 0
; 1: Goto pattern, +8 bytes
TT_USE_GOTO             = 1
; 1: Odd/even rows have different SPEED values, +7 bytes
TT_USE_FUNKTEMPO        = 0
; If the very first notes played on each channel are not PAUSE, HOLD or
; SLIDE, i.e. if they start with an instrument or percussion, then set
; this flag to 0 to save 2 bytes.
; 0: +2 bytes
TT_STARTS_WITH_NOTES    = 0

TT_FREQ_MASK    = %00011111
TT_INS_HOLD     = 8
TT_INS_PAUSE    = 16
TT_FIRST_PERC   = 17

; all symbols visible to other source files

; 0pglobal.s
.globalzp schedule
.globalzp man_l
.globalzp man_r
.globalzp height
.globalzp audio_shadowreg

.globalzp color_topfigures
.globalzp color_centerfigure
.globalzp color_bottomfigures
.globalzp color_text
.globalzp arms_top
.globalzp arms_center
.globalzp arms_bottom

.globalzp tt_timer
.globalzp tt_cur_pat_index_c0
.globalzp tt_cur_pat_index_c1
.globalzp tt_cur_note_index_c0
.globalzp tt_cur_note_index_c1
.globalzp tt_envelope_index_c0
.globalzp tt_envelope_index_c1
.globalzp tt_cur_ins_c0
.globalzp tt_cur_ins_c1
.globalzp tt_ptr

; payload.s
.global   setup0
.global   setup1
.global   setup2
.global   setup3
.global   setup4
.global   setup5
.global   setup6
.global   setup7
.global   setup8

; slocumplayer21.s
.global   psmkPlayer

; main.s
.global   reset
.global   waitvblank   ; called with jsr; x,y untouched
.global   waitscreen   ; called with jsr; x,y untouched
.global   waitoverscan ; called with jmp
; alternative versions, that take use A instead of default for delay
.global   waitvblanka
.global   waitscreena
.global   waitoverscana

; man.s
.global   man1l
.global   man1r
.global   man2l
.global   man2r
.global   manjumptable

; message.s
.global   showmessage

; spritepos.s
.global   spritepos

; splash.s
.if splashtype > 0
.global   splash
.if (splashtype = 3) || (splashtype = 4)
.global   ntscpaltable
.endif
.endif

; ttdata.s
.global   tt_InsADIndexes
.global   tt_InsCtrlTable
.global   tt_InsFreqVolTable
.global   tt_InsReleaseIndexes
.global   tt_InsSustainIndexes
.global   tt_PatternPtrHi
.global   tt_PatternPtrLo
.global   tt_PatternSpeeds
.global   tt_PercCtrlVolTable
.global   tt_PercFreqTable
.global   tt_PercIndexes
.global   tt_SequenceTable

; ttplayer.s
.global   tt_Player

.linecont +
   .define partsaddrlist \
   setup0-1, setup1-1, setup1-1, setup1-1, setup1-1, setup2-1, \
   setup3-1, setup4-1, setup5-1, setup6-1, setup7-1, setup8-1
.linecont -

