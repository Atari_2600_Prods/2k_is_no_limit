
.include "globals.inc"

.segment "RODATA"

manstart:
   .byte $00

man2r:
   .byte %11111100
   .byte %00001100
   .byte %10000100
   .byte %10001100
   .byte %10000100
   .byte %00001100
   .byte %11111100
   .byte %00000100
   .byte %00000110
   .byte %00000001
   .byte %00000011
   .byte %11100001
   .byte %00010111
   .byte %10011001
   .byte %00010001
   .byte %11101110

   .byte $00

man2l:
   .byte %00111111
   .byte %00100001
   .byte %00011100
   .byte %00001000
   .byte %00001000
   .byte %00001000
   .byte %00011111
   .byte %00010000
   .byte %01110000
   .byte %10010000
   .byte %11000000
   .byte %10000111
   .byte %11101000
   .byte %10011010
   .byte %10001000
   .byte %01110111

   .byte $00

man1r:
   .byte %11111100
   .byte %00001100
   .byte %10000100
   .byte %10001100
   .byte %10000111
   .byte %00001001
   .byte %11111111
   .byte %00010001
   .byte %00010011
   .byte %00010001
   .byte %00000001
   .byte %11100010
   .byte %00011100
   .byte %10010000
   .byte %00010000
   .byte %11100000

   .byte $00
   
man1l:
   .byte %00111111
   .byte %00100001
   .byte %00011100
   .byte %00001000
   .byte %00111000
   .byte %01001000
   .byte %11111111
   .byte %10010000
   .byte %10110000
   .byte %10010000
   .byte %10000000
   .byte %01000111
   .byte %00111000
   .byte %00001010
   .byte %00001000
   .byte %00000111

manjumptable:
   .byte $00,$04,$00,$00

manend:
.assert >manend = >manstart, error, "man data must not cross page boundry"
