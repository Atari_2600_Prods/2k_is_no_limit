
.include "vcs.inc"
.include "globals.inc"

.segment "CODE"

waitlinescol:
   sta   COLUBK
waitlines:
   ldy   #$00
   sty   GRP0
   sty   GRP1
:
   dex
   sta   WSYNC
   bne   :-
waitout:
   rts

; input = A
; bit0 right arm up
; bit1 left arm up
; bit2 jump
setman:
   lsr
   bcs   :+
   ldx   #<man1r
   .byte $2c
:
   ldx   #<man2r
   stx   man_r+0
   lsr
   bcs   :+
   ldx   #<man1l
   .byte $2c
:
   ldx   #<man2l
   stx   man_l+0
   ldx   #>man1r  ; must be same for all four sprites
   stx   man_l+1
   stx   man_r+1
   lsr
   bcc   :+
   dec   man_l+0
   dec   man_r+0
:
   rts

setup0:
   ; init
   lda   #$00
   ldx   #$7e
:
   sta   $81,x
   dex
   bpl   :-
   stx   height
   inx
   stx   COLUPF

   ldx   #$31
   stx   PF0
   stx   CTRLPF

   lda   #BGCOL1
   sta   color_topfigures
   lda   #BGCOL3
   sta   color_bottomfigures
   sta   color_text
   
   lda   #$0c
   sta   tt_cur_pat_index_c1

   cli
   jmp   kernel

setup8:
   ; nothing more to add
   dec   schedule
setup7:
   lda   audio_shadowreg+1
   eor   #$ff
   ldx   audio_shadowreg+0
   jmp   setfirgurecolors

setup6:
   lda   #PCOL1
   ldx   #PCOL3
setfirgurecolors:
   sta   color_topfigures
   stx   color_bottomfigures

   lda   tt_cur_note_index_c0
   cmp   #$30
   bcc   :+
   lda   #$03
   sta   arms_top
:

   lda   tt_cur_note_index_c0
   and   #$04
   cmp   #$04
   lda   #$01
   adc   #$00
   sta   arms_bottom
   
setup5:
   ; text color
   lda   tt_cur_note_index_c0
   cmp   #$11
   bcs   :++
   sbc   #$00
   cmp   #$08
   bcc   :+
   eor   #$0f
:
   asl
   ora   #$60
   .byte $2c
:
   lda   #$00
   sta   color_text
   ; move arms
   lda   tt_cur_note_index_c0
   cmp   #$30
   bcc   :+
   lda   #$03
   bne   :++
:
   and   #$04
   cmp   #$04
   lda   #$01
   adc   #$00
:
   sta   arms_center
setup4:
   ; jump
   lda   tt_cur_note_index_c0
   and   #$03
   tay
   ldx   #$02
:
   lda   manjumptable,y
   ora   arms_top,x
   sta   arms_top,x
   dex
   bpl   :-
   bmi   setup1

setup3:
   ; display center figure in full
   lda   tt_cur_note_index_c0
   and   #$18
   cmp   #$08
   bcc   :+
   cmp   #$18
   bcs   :+
   lda   #$03
;   .byte $2c   
:
   sta   arms_center
   jmp   setup1

setup2:
   ; calculate man height from trackpos
   sec
   lda   #<(man1l-man1r-1)*4
   sbc   tt_cur_note_index_c0
.if 0
   bcs   :+
   lda   #$00
:
.endif
   lsr
   lsr
   sta   height

setup1:
   ; just color bars

kernel:
.if 0
   lda   schedule
   asl
   sta   COLUPF
.endif
   jsr   waitvblank
   lda   #BGCOL1
   ldx   #HEIGHT1
   sta   WSYNC
   jsr   waitlinescol

   lda   arms_top
   jsr   setman
   lda   color_topfigures
   jsr   show2men
   sty   arms_top

   lda   #BGCOL2
   ldx   #HEIGHT2
   jsr   waitlinescol

   lda   arms_center
   jsr   setman
   ldx   #$30
   stx   NUSIZ0
   stx   NUSIZ1
.if NTSC
   ldx   #PCOL2
.endif
   stx   COLUP0
   stx   COLUP1
   lda   #$4c
   jsr   spritepos

   ldy   #<(man1l-man1r-1)
:
   cpy   height
   bcc   @clear

   lax   (man_l),y
   lda   (man_r),y
   .byte $2c
@clear:
   lax   #$00
   stx   GRP0
   sta   GRP1
   sta   WSYNC
   sta   WSYNC
   dey
   bpl   :-

   lda   #BGCOL3
   ldx   #$04
   jsr   waitlinescol
   lda   color_text
   jsr   showmessage

   ldx   #HEIGHT3
   jsr   waitlines

   lda   arms_bottom
   jsr   setman
   lda   color_bottomfigures
   jsr   show2men
   sty   arms_center

   lda   #BGCOL4
   ldx   #HEIGHT4
   jsr   waitlinescol
   stx   COLUBK
   jsr   waitscreen
   jsr   tt_Player
   jmp   waitoverscan

show2men:
   sta   COLUP0
   sta   COLUP1
   lda   #$2c
   sta   NUSIZ0
   sta   NUSIZ1
   jsr   spritepos

   ldx   #(<(man1l-man1r-1)*2+1)
:
   txa
   lsr
   tay
   sta   WSYNC

   lda   #$08        ; 2
   sta   REFP0       ; 3
   sta   REFP1       ; 3
   lda   (man_r),y   ; 5
   sta   GRP0        ; 3
   lda   (man_l),y   ; 5
   sta   GRP1        ; 3
   jsr   @delay12

   lda   #$00
   sta   REFP0
   sta   REFP1
   lda   (man_l),y   ; 5
   sta   GRP0        ; 3
   lda   (man_r),y   ; 5
   sta   GRP1        ; 3
   dex               ; 2
   bpl   :-          ; 3
@delay12:
   rts

