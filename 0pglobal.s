
.include "globals.inc"

.segment "ZEROPAGE"

; must be first, at $80
schedule:
   .byte 0
man_l:
   .word 0
man_r:
   .word 0
height:
   .byte 0
audio_shadowreg:
   .word 0
color_topfigures:
   .byte 0
color_bottomfigures:
   .byte 0
color_text:
   .byte 0
arms_top:
   .byte 0
arms_center:
   .byte 0
arms_bottom:
   .byte 0
tt_cur_pat_index_c0: ; current pattern index into tt_SequenceTable
   .byte 0
tt_cur_pat_index_c1:
   .byte 0
tt_cur_note_index_c0: ; note index into current pattern
   .byte 0
tt_cur_note_index_c1:
   .byte 0
tt_envelope_index_c0: ; index into ADSR envelope
   .byte 0
tt_envelope_index_c1:
   .byte 0
tt_cur_ins_c0: ; current instrument
   .byte 0
tt_cur_ins_c1:
   .byte 0
tt_timer: ; current music timer value
   .byte 0
tt_ptr: ; temporary pointer
   .word 0

localramstart:
