
.include "globals.inc"
.include "vcs.inc"

.segment "CODE"

spritepos:
; x = sprite to set
; a = position
   sta WSYNC
   sta HMCLR
   sec

@loop:
   sbc   #$0f
   bcs   @loop
   eor   #$07
   asl
   asl
   asl
   asl
   sta   RESP0 ; +$2000
   sta   RESP1
   sta   HMP0
   adc   #$10
   sta   HMP1
   sta   WSYNC
   sta   HMOVE
   rts
